input = File.read('input.txt').split("\n").map(&:to_i)

puts input.each_cons(3).map(&:sum).each_cons(2).select { |a, b| a < b }.count

input = File.read('input.txt').split("\n")
h_pos, depth = 0, 0
input.each do |action|
  act, value = action.split(' ')
  case act
  when 'forward'
    h_pos += value.to_i
  when 'up'
    depth -= value.to_i
  else
    depth += value.to_i
  end
end
puts h_pos * depth
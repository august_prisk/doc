input = File.read('input.txt').split("\n")
h_pos, depth, aim = 0, 0, 0
input.each do |action|
  act, value = action.split(' ')
  case act
  when 'forward'
    h_pos += value.to_i
    depth += value.to_i * aim unless aim.zero?
  when 'up'
    aim -= value.to_i
  else
    aim += value.to_i
  end
end
puts h_pos * depth